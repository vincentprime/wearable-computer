/*
 * Tricorder Sensor Collector
 */

#include "AirQuality.h"
#include "Arduino.h"
#include <LiquidCrystal.h>
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

AirQuality airqualitysensor;
int current_quality = -1;
int last_data = -1;

int analogMeter = 0;

const int pingPin = 7; // Sound distance pin
//Accel pins
const int xPin = 8;
const int yPin = 9;

const int meterPin = 6;
const int dialPin = 1;

void setup() {
  lcd.begin(16, 2);
  lcd.print("Booting");
  Serial.begin(9600);
  
  airqualitysensor.init(14);

  pinMode(xPin, INPUT);
  pinMode(yPin, INPUT);
  
  pinMode(meterPin, OUTPUT);
  
}

void loop() {
  // put your main code here, to run repeatedly:
  String bk = "|";

  float dial = analogRead(dialPin);

  //int dialAdj/255 = dial/1024;
  float dialAdj = (dial * 255) / 1024;
  
  int distance = getDistance();

  float distanceAdj = (distance * 255) / dialAdj;
  
  String distanceMark = "cm";
  String distanceLang = distance + distanceMark;
  
  
  current_quality=airqualitysensor.slope();
  if (current_quality >= 0) {
    last_data = current_quality;
  }
  String qualityMark = "aql ";
  String qualityLang = last_data + qualityMark;

  int accel[2];
  getAccel(accel);

  
  
  analogWrite(meterPin, distanceAdj);

  String xMark = "X ";
  String yMark = "Y";
  String accelLang = accel[0] + xMark + accel[1] + yMark;
  
  String dialMark = "val|";
  String dialLang = (int)distanceAdj + bk + (int)dialAdj;
  
  String lineOne = dialLang + distanceLang;
  String lineTwo = qualityLang + bk + accelLang;
  lcd.clear();
  lcd.print(lineOne);
  
  lcd.setCursor(0,1);
  lcd.print(lineTwo);
  
  delay(100);
}

int getAccel(int accel[2]) {
  int pulseX, pulseY;

  int accelX, accelY;

  pulseX = pulseIn(xPin, HIGH);
  pulseY = pulseIn(yPin, HIGH);

  //Convert pulse width to accel
  // these are in mili-g's
  accel[0] = ((pulseX / 10) - 500) * 8;
  accel[1] = ((pulseY / 10) - 500) * 8;
  
  return accel;
}

long getDistance() {
  long cm;

  // Trigger the sensor
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);

  // Read the sensor
  // The duration of the high pulse is the time in microseconds of the echo
  pinMode(pingPin, INPUT);
  long duration = pulseIn(pingPin, HIGH);

  return microsecondsToCentimeters(duration);
}

long microsecondsToCentimeters( long microseconds ) {
  // Sound travels at 29 microseconds per centimeter
  // Divide it by two to get the distance
  return microseconds / 29 / 2;
}

ISR(TIMER1_OVF_vect)
{
  if(airqualitysensor.counter==61)//set 2 seconds as a detected duty
  {

      airqualitysensor.last_vol=airqualitysensor.first_vol;
      airqualitysensor.first_vol=analogRead(A0);
      airqualitysensor.counter=0;
      airqualitysensor.timer_index=1;
      PORTB=PORTB^0x20;
  }
  else
  {
    airqualitysensor.counter++;
  }
}
